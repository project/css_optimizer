<?php

namespace Drupal\css_optimizer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class CssAlterForm extends ConfigFormBase
{

    // Using the first basic method to associate an ID with form 
    public function getFormId()
    {
        return 'css_optimizer_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
        'css_optimizer.settings',
        ];
    }


    // calling method to build form 
    // we build the complete form in form of an array
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $activeTheme = \Drupal::config('system.theme')->get('default');
        if (!empty(\Drupal::service('theme_handler')->listInfo()[$activeTheme]->base_themes)) {
            $base_themes = \Drupal::service('theme_handler')->listInfo()[$activeTheme]->base_themes;
            $libraryDiscovery = \Drupal::service('library.discovery');

            $config = $this->config('css_optimizer.settings');

            $default_values = $config->getRawData();

            $trueBoxes = array();
            foreach ($default_values as $key => $value) {
                $trueBoxes[$value] = true;
            }

            $header = [
            'themename' => t('Theme Name'),
            'themelibrary' => t('Theme Library'),
            ];

            foreach ($base_themes as $machineName => $themeName) {
                $library[$machineName] = $libraryDiscovery->getLibrariesByExtension($machineName);
            }

            // Initialising empty array and setting value of i 
            $output = [];
            $i = 1;
            foreach ($library as $key => $value) {
                foreach ($value as $themeDetails) {
                    foreach ($themeDetails['css'] as $depthDetails) {
                        // generate a custom key 
                        $customKey = $i . '__' . $key . '__' . $depthDetails['data'];
                        $output[$customKey] = [
                        'themename' => $key,
                        'themelibrary' => $depthDetails['data'],
                        ];
                        $i++;

                    }
                }
            }

            $form['table'] = [
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $output,
            '#empty' => t('No Themes found'),
            '#weight' => 10,
            '#default_value' => $trueBoxes,
            ];

            return parent::buildForm($form, $form_state);
        }
    }

    // Calling submit method now 
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('css_optimizer.settings');
        $config->delete();
        $checkedValues = $form_state->getValue('table') ?: '';
        $index = 1;
        
        foreach ($checkedValues as $key => $value) {
            if ($key == $value) {
                $key = 'theme__' . $index;
                $config->set($key, $value);
                $index++;
                $config->save();
            }
        }
        \Drupal::messenger()->addMessage('The Configurations have been updated', 'status');
    }
} 