CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

CSS Optimizer intends to improves the perfromance of drupal site by removing unused css files.


REQUIREMENTS
------------

Works with Drupal 8 and 9, no special requirements.


INSTALLATION
------------

 * Install as you normally install any contributed module


CONFIGURATION
-------------

 * The configuration form is available at (/admin/config/css_optimizer).
 * Once you have selected the files, which you don't want to load, export configurations as you normally do.


MAINTAINERS
-----------

Current maintainers:
 * Lakshay (codewithlakshay) - https://www.drupal.org/u/codewithlakshay
